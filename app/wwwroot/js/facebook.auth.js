﻿/*
** facebook external authentication
*/

window.fbAsyncInit = function () {
  FB.init({
    appId: $('#facebookAppId').val(),
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v8.0'
  });
};


FB.getLoginStatus(function (response) {
  statusChangeCallback(response);
});
function checkLoginState() {
  FB.getLoginStatus(function (response) {
    statusChangeCallback(response);
  });
}
function statusChangeCallback(response) {
  if (response.status === 'connected') {
    var accessToken = response.authResponse.accessToken;
    var userId = response.authResponse.userID;
    FB.api('/me?fields=id,name,email,permissions', function (response) {
      $.post("/Customer/FacebookLoginCallback", { accessToken: accessToken, email: response.email, name: response.name, userId: userId }, function (data) {
        if (data.success) {
          location.href = '/';
        }
      });
    });
  } else {
    console.log(response)
  }
}